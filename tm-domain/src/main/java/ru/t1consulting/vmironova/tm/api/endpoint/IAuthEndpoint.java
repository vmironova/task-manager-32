package ru.t1consulting.vmironova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.dto.request.UserLoginRequest;
import ru.t1consulting.vmironova.tm.dto.request.UserLogoutRequest;
import ru.t1consulting.vmironova.tm.dto.request.UserViewProfileRequest;
import ru.t1consulting.vmironova.tm.dto.response.UserLoginResponse;
import ru.t1consulting.vmironova.tm.dto.response.UserLogoutResponse;
import ru.t1consulting.vmironova.tm.dto.response.UserViewProfileResponse;

public interface IAuthEndpoint {

    @NotNull
    UserLoginResponse userLogin(@NotNull UserLoginRequest request);

    @NotNull
    UserLogoutResponse userLogout(@NotNull UserLogoutRequest request);

    @NotNull
    UserViewProfileResponse userViewProfile(@NotNull UserViewProfileRequest request);

}
