package ru.t1consulting.vmironova.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.model.User;

public final class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@Nullable final User user) {
        super(user);
    }

}
