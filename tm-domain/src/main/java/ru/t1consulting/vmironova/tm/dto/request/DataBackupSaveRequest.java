package ru.t1consulting.vmironova.tm.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class DataBackupSaveRequest extends AbstractUserRequest {
}
