package ru.t1consulting.vmironova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.dto.request.ApplicationAboutRequest;
import ru.t1consulting.vmironova.tm.dto.request.ApplicationVersionRequest;
import ru.t1consulting.vmironova.tm.dto.response.ApplicationAboutResponse;
import ru.t1consulting.vmironova.tm.dto.response.ApplicationVersionResponse;

public interface ISystemEndpoint {

    @NotNull ApplicationAboutResponse applicationAbout(@NotNull ApplicationAboutRequest request);

    @NotNull ApplicationVersionResponse applicationVersion(@NotNull ApplicationVersionRequest request);

}
