package ru.t1consulting.vmironova.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public interface IDomainEndpointClient extends IDomainEndpoint {

    void setSocket(@Nullable Socket socket);

}
