package ru.t1consulting.vmironova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IAuthEndpointClient getAuthEndpointClient();

    @NotNull
    IDomainEndpointClient getDomainEndpointClient();

    @NotNull
    IProjectEndpointClient getProjectEndpointClient();

    @NotNull
    ISystemEndpointClient getSystemEndpointClient();

    @NotNull
    ITaskEndpointClient getTaskEndpointClient();

    @NotNull
    IUserEndpointClient getUserEndpointClient();

    @NotNull
    IEndpointClient getConnectionEndpointClient();

}
