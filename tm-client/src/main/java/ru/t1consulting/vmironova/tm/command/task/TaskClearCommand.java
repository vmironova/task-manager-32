package ru.t1consulting.vmironova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.dto.request.TaskClearRequest;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Remove all tasks.";

    @NotNull
    public static final String NAME = "task-clear";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        @NotNull final TaskClearRequest request = new TaskClearRequest();
        getTaskEndpoint().taskClear(request);
    }

}
