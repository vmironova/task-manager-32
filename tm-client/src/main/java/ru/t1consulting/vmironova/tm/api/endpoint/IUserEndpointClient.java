package ru.t1consulting.vmironova.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public interface IUserEndpointClient extends IUserEndpoint {

    void setSocket(@Nullable Socket socket);

}
